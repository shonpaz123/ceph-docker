mkdir -p /etc/ceph 
mkdir -p /var/lib/ceph 
sudo chcon -Rt svirt_sandbox_file_t /etc/ceph
sudo chcon -Rt svirt_sandbox_file_t /var/lib/ceph
sudo docker run -d --name mon --restart=always --net=host -v /etc/ceph:/etc/ceph -v /var/lib/ceph/:/var/lib/ceph/ -e MON_IP=192.168.42.101 -e CEPH_PUBLIC_NETWORK=192.168.42.0/24 -e CEPH_CLUSTER_NETWORK=192.168.43.0/24  registry.access.redhat.com/rhceph/rhceph-3-rhel7 mon
sleep 5
docker stop mon 
echo 'osd pool default size = 1' >> /etc/ceph/ceph.conf
echo 'osd pool default min_size = 1' >> /etc/ceph/ceph.conf
docker start mon 
sleep 10
sudo docker run -d --name mgr --net=host -v /etc/ceph:/etc/ceph -v /var/lib/ceph/:/var/lib/ceph/ registry.access.redhat.com/rhceph/rhceph-3-rhel7 mgr
sleep 5
docker run -d --name rgw --net=host --restart=always -v /var/lib/ceph/:/var/lib/ceph/ -v /etc/ceph:/etc/ceph registry.access.redhat.com/rhceph/rhceph-3-rhel7 rgw
sleep 5
docker run -d --name osd --net=host --pid=host --privileged=true -v /etc/ceph:/etc/ceph -v /var/lib/ceph/:/var/lib/ceph/ -v /dev/:/dev/ -e OSD_DEVICE=/dev/sdb registry.access.redhat.com/rhceph/rhceph-3-rhel7 osd
sleep 10 
docker exec -it mon ceph mgr module enable dashboard 
sleep 5
docker exec -it mon ceph mgr module enable retful 
sleep 5
docker exec -it mon ceph mgr module enable iostat 
sleep 5
docker exec -it mon radosgw-admin user create --uid=shon --display-name=shon --access-key=shon --secret-key=shon 
sleep 5




