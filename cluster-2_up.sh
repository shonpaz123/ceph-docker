mkdir -p /etc/ceph-2 
mkdir -p /var/lib/ceph-2 
sudo chcon -Rt svirt_sandbox_file_t /etc/ceph-2
sudo chcon -Rt svirt_sandbox_file_t /var/lib/ceph-2
sudo docker run -d --name mon-cluster2 --restart=always --net=host -v /etc/ceph-2:/etc/ceph -v /var/lib/ceph-2/:/var/lib/ceph/ -e MON_IP=192.168.42.101:3400 -e CEPH_PUBLIC_NETWORK=192.168.42.0/24 -e CEPH_CLUSTER_NETWORK=192.168.43.0/24  registry.access.redhat.com/rhceph/rhceph-3-rhel7 mon
sleep 5
docker stop mon-cluster2
echo 'osd pool default size = 1' >> /etc/ceph-2/ceph.conf
echo 'osd pool default min_size = 1' >> /etc/ceph-2/ceph.conf
docker start mon-cluster2
sleep 10
sudo docker run -d --name mgr-cluster2 --net=host -v /etc/ceph-2:/etc/ceph -v /var/lib/ceph-2/:/var/lib/ceph/ registry.access.redhat.com/rhceph/rhceph-3-rhel7 mgr
sleep 5
docker run -d --name rgw-cluster2 --net=host --restart=always -v /var/lib/ceph-2/:/var/lib/ceph/ -v /etc/ceph-2:/etc/ceph registry.access.redhat.com/rhceph/rhceph-3-rhel7 rgw
sleep 5
docker run -d --name osd-cluster2 --net=host --pid=host --privileged=true -v /etc/ceph-2:/etc/ceph -v /var/lib/ceph-2/:/var/lib/ceph/ -v /dev/:/dev/ -e OSD_DEVICE=/dev/sdc registry.access.redhat.com/rhceph/rhceph-3-rhel7 osd




